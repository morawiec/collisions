export function getWorldData(windowObj, objectsInput, equalMassInput, command = 'init') {
  const objects = Number.parseInt(objectsInput.value);
  const equalMass = equalMassInput.checked;
  return {
    x: windowObj.innerWidth,
    y: windowObj.innerHeight,
    objects: objects,
    equal_mass: equalMass,
    command: command
  };
}

export function sendWorldCommand(websocketsClient, worldData) {
  websocketsClient.send(JSON.stringify(worldData));
}

export function updateWorldSize(canvas, width, height) {
  canvas.width = width;
  canvas.height = height;
}
