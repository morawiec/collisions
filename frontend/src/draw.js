export function draw(canvas, context, objects) {
  context.clearRect(0, 0, canvas.width, canvas.height);
  objects.forEach((circle) => drawCircle(context, circle));
}

export function uniqueColors(count) {
  let colors = [];

  for (let i = 0; i < count; i++) {
    const color = '#' + Math.floor(Math.random() * 16777215).toString(16);
    colors.push(color);
  }

  return colors;
}

function drawCircle(context, circle) {
  context.beginPath();
  context.arc(...circle.coordinates, circle.radius, 0, 2 * Math.PI);
  context.fillStyle = "#800000";
  context.fill();
  context.closePath();
}
