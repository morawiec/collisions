import style from './main.css'
import {draw, uniqueColors} from './draw.js'
import {getWorldData, sendWorldCommand, updateWorldSize} from './world.js';

// Just so PyCharm reformat does not remove css styles
console.log(style);

const websocketsClient = new WebSocket('ws://127.0.0.1:8090');

const switchButton = document.getElementById('toggle-button');
const initButton = document.getElementById('init');
const playButton = document.getElementById('play');
const pauseButton = document.getElementById('pause');

let menu = document.getElementById('menu');
menu.style.display = 'none';
let menuHidden = true;

const objectsInput = document.getElementById('objects');
const equalMassInput = document.getElementById('equal-mass');

const canvas = document.getElementById('world');
updateWorldSize(canvas, window.innerWidth, window.innerHeight);
const ctx = canvas.getContext('2d');

window.onresize = (event) => {
  const data = getWorldData(event.target, objectsInput, equalMassInput, 'init');
  updateWorldSize(canvas, event.target.innerWidth, event.target.innerHeight);
  sendWorldCommand(websocketsClient, data);
};

websocketsClient.onopen = (event) => {
  const data = getWorldData(window, objectsInput, equalMassInput, 'init');
  sendWorldCommand(websocketsClient, data);
};

websocketsClient.onmessage = (event) => {
  const objects = JSON.parse(event.data);
  draw(canvas, ctx, objects);
};

switchButton.onclick = (event) => {
  if (menuHidden) {
    menu.style.display = 'block';
  } else {
    menu.style.display = 'none';
  }

  menuHidden = !menuHidden;
};

initButton.onclick = (event) => {
  const data = getWorldData(window, objectsInput, equalMassInput, 'init');
  sendWorldCommand(websocketsClient, data);
};

playButton.onclick = (event) => {
  const data = getWorldData(window, objectsInput, equalMassInput, 'play');
  sendWorldCommand(websocketsClient, data);
};

pauseButton.onclick = (event) => {
  const data = getWorldData(window, objectsInput, equalMassInput, 'pause');
  sendWorldCommand(websocketsClient, data);
};
