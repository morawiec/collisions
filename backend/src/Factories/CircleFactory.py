from random import randint

from src.Models.Circle import Circle


class CircleFactory:
    RADIUS = 80
    MASS = 10
    VELOCITY = 10

    @staticmethod
    def create(max_x, max_y, number_of_objects, equal_mass):
        objects = []
        for _ in range(number_of_objects):
            if equal_mass:
                coordinates = [randint(CircleFactory.RADIUS, max_x - CircleFactory.RADIUS),
                               randint(CircleFactory.RADIUS, max_y - CircleFactory.RADIUS)]

                circle = Circle(CircleFactory.MASS, coordinates, CircleFactory.RADIUS)
            else:

                params = CircleFactory._calculate_params()
                coordinates = [randint(params["radius"], max_x - params["radius"]),
                               randint(params["radius"], max_y - params["radius"])]

                circle = Circle(params["mass"], coordinates, params["radius"])

            circle.set_velocity(
                randint(-1 * CircleFactory.VELOCITY, CircleFactory.VELOCITY),
                randint(-1 * CircleFactory.VELOCITY, CircleFactory.VELOCITY)
            )
            objects.append(circle)

        return objects

    @staticmethod
    def _calculate_params():
        mass = randint(1, CircleFactory.MASS)
        return {"mass": mass,
                "radius": 5*mass
                }

