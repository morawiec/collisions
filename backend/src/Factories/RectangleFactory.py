from src.Models.Border import Border
from src.Models.Rectangle import Rectangle
from src.Utils.Tags import DirectionTag


class RectangleFactory:
    @staticmethod
    def create(width, height):
        rectangle = Rectangle(width, height)

        RectangleFactory._set_borders(width, height, rectangle)

        return rectangle

    @staticmethod
    def _set_borders(width, height, rectangle):
        rectangle.borders["upper_border"] = Border(width, DirectionTag.HORIZONTAL)
        rectangle.borders["bottom_border"] = Border(width, DirectionTag.HORIZONTAL)
        rectangle.borders["left_border"] = Border(height, DirectionTag.VERTICAL)
        rectangle.borders["right_border"] = Border(height, DirectionTag.VERTICAL)

        return rectangle
