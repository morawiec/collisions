class CollisionMemory:
    def __init__(self):
        self.collision_memory = set()

    def add_collision(self, object_a, object_b):
        self.collision_memory.add((object_a, object_b))
        self.collision_memory.add((object_b, object_a))

    def check_if_collision_already_occurred(self, object_a, object_b):
        return (object_a, object_b) in self.collision_memory

    def check_if_collision_did_not_occur(self, object_a, object_b):
        return not self.check_if_collision_already_occurred(object_a, object_b)

    def reset(self):
        self.collision_memory = set()
