from scipy.spatial import distance

from src.Utils.CircleWrapper import CircleWrapper


class CollisionDetectorHelper:
    @staticmethod
    def find_nearby_circles(circle, circle_list, num):
        examined = []
        for examined_circle in circle_list:
            if circle != examined_circle:
                distance = CollisionDetectorHelper.calculate_distance(circle, examined_circle)
                examined.append(CircleWrapper(circle, examined_circle, distance))

        found = sorted(examined, key=lambda wrapped: wrapped.distance)
        return found[:num]

    @staticmethod
    def is_collision_with_circle(circle_wrapper):
        radius_sum = circle_wrapper.circle.radius + circle_wrapper.examined_circle.radius

        return circle_wrapper.distance <= radius_sum

    @staticmethod
    def get_colliding_borders(boundary, object):
        new_x, new_y = object.try_move()

        boundaries = []

        if new_x < 0:
            boundaries.append(boundary.borders["left_border"])

        if new_x > boundary.width:
            boundaries.append(boundary.borders["right_border"])

        if new_y < 0:
            boundaries.append(boundary.borders["bottom_border"])

        if new_y > boundary.height:
            boundaries.append(boundary.borders["upper_border"])

        return boundaries if len(boundaries) > 0 else None

    @staticmethod
    def calculate_distance(object_a, object_b):
        a = (object_a.center_coordinates[0], object_a.center_coordinates[1])
        b = (object_b.center_coordinates[0], object_b.center_coordinates[1])
        return distance.euclidean(a, b)
