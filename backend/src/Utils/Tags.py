from enum import Enum


class CollisionTag(Enum):
    TwoDimensionCollision = "Two dimension collision"
    BorderCollision = "BorderCollision"


class DirectionTag(Enum):
    HORIZONTAL = "HORIZONTAL"
    VERTICAL = "VERTICAL"
