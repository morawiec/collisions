import json


class MessageHandler:
    @staticmethod
    def parse(message):
        data = json.loads(message)

        return data["x"], data["y"], data["command"], data["objects"], data["equal_mass"]

    @staticmethod
    def to_json(objects):
        data = [{'coordinates': circle_object.center_coordinates, 'radius': circle_object.radius,
                 'mass': circle_object.mass} for circle_object in objects]

        return json.dumps(data)
