from src.Models.Circle import Circle
from src.Physics.Collision import Collision
from src.Utils.CollisionDetectorHelper import CollisionDetectorHelper
from src.Utils.CollisionMemory import CollisionMemory
from src.Utils.Tags import CollisionTag


class CircleController:
    def __init__(self, boundary, circle_list):
        self.boundary = boundary
        self.circle_list = circle_list
        self.memory = CollisionMemory()

    def update_position(self):
        self._init_memory()

        for circle in self.circle_list:
            borders = CollisionDetectorHelper.get_colliding_borders(self.boundary, circle)
            if borders is not None:
                for border in borders:
                    self._collision_with_border(border, circle)
                continue

            nearby_circles = CollisionDetectorHelper.find_nearby_circles(circle, self.circle_list, 2)
            if not nearby_circles:
                self._no_collision(circle)
                continue

            for circle_wrapper in nearby_circles:
                if self._collision_occurs(circle_wrapper):
                    self._collision_with_object(circle_wrapper)
                else:
                    self._no_collision(circle)

    def _init_memory(self):
        self.memory.reset()

    @staticmethod
    def _no_collision(circle):
        circle.move()

    def _collision_with_object(self, circle_wrapper):
        Collision.collide(circle_wrapper.circle, circle_wrapper.examined_circle,
                          CollisionTag.TwoDimensionCollision)
        self.memory.add_collision(circle_wrapper.circle, circle_wrapper.examined_circle)
        circle_wrapper.circle.move()
        circle_wrapper.examined_circle.move()

    @staticmethod
    def _collision_with_border(border, circle: Circle):
        Collision.collide(circle, border, CollisionTag.BorderCollision)
        circle.move()

    def _collision_occurs(self, circle_wrapper):
        collision_with_circle = CollisionDetectorHelper.is_collision_with_circle(circle_wrapper)
        collision_did_not_occur = self.memory.check_if_collision_did_not_occur(
            circle_wrapper.circle,
            circle_wrapper.examined_circle
        )

        return collision_with_circle and collision_did_not_occur
