from src.Physics.CollisionsStrategies import CollisionStrategyContext


class Collision:
    @staticmethod
    def collide(object_a, object_b, collision_tag):
        context = CollisionStrategyContext(collision_tag)
        context.strategy.collide(object_a, object_b)
