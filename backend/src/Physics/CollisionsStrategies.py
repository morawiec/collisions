import math

import numpy as np

from src.Abstracts.AbstractCollisionStrategy import AbstractCollisionStrategy
from src.Models.Border import Border
from src.Models.Circle import Circle
from src.Utils.Tags import CollisionTag
from src.Utils.Tags import DirectionTag


class CollisionStrategyContext:
    def __init__(self, collision_tag):
        if collision_tag == CollisionTag.TwoDimensionCollision:
            self.strategy = TwoDimensionCollision()
        elif collision_tag == CollisionTag.BorderCollision:
            self.strategy = BorderCollision()
        else:
            raise Exception('Unsupported CollisionTag')


class BorderCollision(AbstractCollisionStrategy):
    @staticmethod
    def collide(object_a: Circle, object_b: Border):
        x_coordinate = 0
        y_coordinate = 1

        if type(object_b) != Border:
            raise Exception('Border collision expected')

        elif object_b.direction == DirectionTag.VERTICAL:
            object_a.velocity = [-object_a.velocity[x_coordinate], object_a.velocity[y_coordinate]]

        elif object_b.direction == DirectionTag.HORIZONTAL:
            object_a.velocity = [object_a.velocity[x_coordinate], -object_a.velocity[y_coordinate]]


class TwoDimensionCollision(AbstractCollisionStrategy):
    def collide(self, object_a: Circle, object_b: Circle):
        normal_vector = self._calculate_normal_vector(object_a, object_b)
        unit_normal_vector = self._calculate_unit_normal_vector(normal_vector)
        unit_tangent_vector = self._calculate_unit_tangent_vector(unit_normal_vector)

        object_a.velocity, object_b.velocity = self._calculate_new_velocity(
            object_a, object_b, unit_normal_vector, unit_tangent_vector
        )

    @staticmethod
    def _calculate_unit_normal_vector(normal_vector):
        magnitude = math.sqrt(pow(normal_vector[0], 2) + pow(normal_vector[1], 2))
        return [normal_vector[0] / magnitude, normal_vector[1] / magnitude]

    @staticmethod
    def _calculate_unit_tangent_vector(normal_vector):
        return [- normal_vector[1], normal_vector[0]]

    @staticmethod
    def _calculate_normal_vector(object_a: Circle, object_b: Circle):
        return [object_b.center_coordinates[0] - object_a.center_coordinates[0],
                object_b.center_coordinates[1] - object_a.center_coordinates[1]]

    @staticmethod
    def _calculate_new_velocity(object_a: Circle, object_b: Circle, unit_normal_vector, unit_tangent_vector):
        object_a_normal = np.dot(unit_normal_vector, object_a.velocity)
        object_a_tangential = np.dot(unit_tangent_vector, object_a.velocity)

        object_b_normal = np.dot(unit_normal_vector, object_b.velocity)
        object_b_tangential = np.dot(unit_tangent_vector, object_b.velocity)

        new_velocity_a_scalar = (object_a_normal * (
                object_a.mass - object_b.mass) + 2 * object_b.mass * object_b_normal) / (
                                        object_a.mass + object_b.mass)
        new_velocity_b_scalar = (object_b_normal * (
                object_b.mass - object_a.mass) + 2 * object_a.mass * object_a_normal) / (
                                        object_a.mass + object_b.mass)

        new_velocity_a_vector_normal = new_velocity_a_scalar * np.array(unit_normal_vector)
        new_velocity_b_vector_normal = new_velocity_b_scalar * np.array(unit_normal_vector)

        new_velocity_a_vector_tangent = object_a_tangential * np.array(unit_tangent_vector)
        new_velocity_b_vector_tangent = object_b_tangential * np.array(unit_tangent_vector)

        new_velocity_a = np.add(new_velocity_a_vector_normal, new_velocity_a_vector_tangent)
        new_velocity_b = np.add(new_velocity_b_vector_normal, new_velocity_b_vector_tangent)

        return [new_velocity_a, new_velocity_b]
