from src.Controllers.CircleController import CircleController
from src.Factories.RectangleFactory import RectangleFactory


class World:
    def __init__(self):
        self.time = 0
        self.boundary = None
        self.objects = list()
        self.controller = None
        self._running = False

    def handle(self, command, x, y, objects):
        if command == 'init':
            self.initialize(x, y, objects)
        elif command == 'play':
            self.enable()
        elif command == 'pause':
            self.disable()
        else:
            raise Exception('Unsupported command!')

    def initialize(self, x, y, objects):
        self.time = 0
        self.boundary = RectangleFactory.create(x, y)
        self.objects = objects
        self.controller = CircleController(self.boundary, self.objects)
        self.enable()

    def enable(self):
        self._running = True

    def disable(self):
        self._running = False

    def running(self):
        return True

    def process(self):
        if not self._running:
            return

        self.time += 1
        self.controller.update_position()
