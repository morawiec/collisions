from abc import ABCMeta, abstractmethod


class AbstractCollisionStrategy(metaclass=ABCMeta):
    @abstractmethod
    def collide(self, object_a, object_b):
        pass
