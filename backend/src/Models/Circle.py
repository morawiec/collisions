from operator import add


class Circle:
    def __init__(self, mass, coordinates, radius):
        self.velocity = list()
        self.center_coordinates = coordinates
        self.mass = mass
        self.radius = radius

    def set_velocity(self, x, y):
        self.velocity.append(x)
        self.velocity.append(y)

    def try_move(self):
        return list(map(add, self.center_coordinates, self.velocity))

    def move(self):
        self.center_coordinates = self.try_move()
