import time

from src.WebSockets.Server import Server
from src.World.World import World

if __name__ == "__main__":
    world = World()
    ws_server = Server()
    ws_server.handle(world)
    ws_server.start()

    while world.running():
        world.process()
        ws_server.send_objects(world.objects)
        # 60 fps
        time.sleep(1/60)
